# Development

**Selected concept:**  Domain-Driven Development

**API Architecture style:** REST

**Platform:** .NET Core



## Tech Stack

- ASP.NET Core Web API
- Entity Framework Core - Code First
- MS SQL Server
- AutoMapper
- Newtonsoft.Json



## Tools

- MS Visual Studio 2022
- Visual Studio Code
- SSMS 2019
- Postman
- Draw.io 
- Typora



## Further steps

- Extend functionality by complicating domain layer
- Cover with unit and integration tests. Using Moq and NUnit packages is recommended
- Apply microservices arch, by splitting APIs among different projects
  - <u>Create/Manage/Store Customer Requests functionality</u> split to **Request.API** project by using **Redis** because it will increase response speed and provide caching features in order to decrease the loud from central db
  - <u>Create/Manage/Store Car and Quotations functionality</u> split to **Catalog.API** project by using **MongoDB** because car-related domain can extend in future, therefore document-oriented model application is a proper solution
  - Transmit user-related functionality to **Users.API** project by remaining **MS SQL Server** as a dbms for consistency in the whole project
  - Transmit security and identity functions to **Identity.API** by using **PostgreSQL** which provides a higher security, performance and consistency levels