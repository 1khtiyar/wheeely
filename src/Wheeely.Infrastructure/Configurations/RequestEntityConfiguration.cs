﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Wheeely.Domain.Entities;

namespace Wheeely.Infrastructure.Configurations
{
    public class RequestEntityConfiguration : IEntityTypeConfiguration<RequestEntity>
    {
        public void Configure(EntityTypeBuilder<RequestEntity> builder)
        {
            builder.HasKey(c => c.ID);


            builder.Property(c => c.ID)
                .ValueGeneratedOnAdd();

            builder.Property(c => c.Message)
                .HasMaxLength(1000);

        }
    }
}
