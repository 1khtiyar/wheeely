﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Wheeely.Domain.Entities;

namespace Wheeely.Infrastructure.Configurations
{
    public class CustomerEntityConfiguration : IEntityTypeConfiguration<CustomerEntity>
    {
        public void Configure(EntityTypeBuilder<CustomerEntity> builder)
        {
            builder.HasKey(c => c.ID);


            builder.Property(c => c.ID)
                .ValueGeneratedOnAdd();

            builder.Property(c => c.FirstName)
                .HasMaxLength(32);

            builder.Property(c => c.LastName)
                .HasMaxLength(32);


            builder.HasMany(c => c.Requests)
                .WithOne(r => r.Customer)
                .HasForeignKey(r=>r.CustomerID)
                .OnDelete(DeleteBehavior.Cascade);

        }
    }
}
