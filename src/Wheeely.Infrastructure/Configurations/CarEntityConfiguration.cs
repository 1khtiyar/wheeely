﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Wheeely.Domain.Entities;

namespace Wheeely.Infrastructure.Configurations
{
    public class CarEntityConfiguration : IEntityTypeConfiguration<CarEntity>
    {
        public void Configure(EntityTypeBuilder<CarEntity> builder)
        {
            builder.HasKey(c => c.ID);


            builder.Property(c => c.ID)
                .ValueGeneratedOnAdd();

            builder.Property(c => c.Name)
                .HasMaxLength(64)
                .IsRequired();

            builder.Property(c => c.Year)
                .IsRequired();


            builder.HasOne(c => c.Quotation)
                .WithOne(q => q.Car)
                .HasForeignKey<QuotationEntity>(q => q.CarID);
        }
    }
}
