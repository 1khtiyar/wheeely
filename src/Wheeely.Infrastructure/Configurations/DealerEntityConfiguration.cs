﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Wheeely.Domain.Entities;

namespace Wheeely.Infrastructure.Configurations
{
    public class DealerEntityConfiguration : IEntityTypeConfiguration<DealerEntity>
    {
        public void Configure(EntityTypeBuilder<DealerEntity> builder)
        {
            builder.HasKey(d => d.ID);


            builder.Property(d => d.ID)
                .ValueGeneratedOnAdd();

            builder.Property(d => d.FirstName)
                .HasMaxLength(32);

            builder.Property(d => d.LastName)
                .HasMaxLength(32);


            builder.HasMany(d => d.Cars)
                .WithOne(c => c.Owner)
                .HasForeignKey(c => c.OwnerID)
                .OnDelete(DeleteBehavior.Cascade);

        }
    }
}
