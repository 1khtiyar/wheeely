﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Wheeely.Domain.Entities;

namespace Wheeely.Infrastructure.Configurations
{
    public class QuotationEntityConfiguration : IEntityTypeConfiguration<QuotationEntity>
    {
        public void Configure(EntityTypeBuilder<QuotationEntity> builder)
        {
            builder.HasKey(q => q.ID);


            builder.Property(q => q.Price)
                .IsRequired();

            builder.HasOne(q => q.Currency)
                .WithMany(c => c.Quotations)
                .HasForeignKey(q => q.CurrencyID);


        }
    }
}
