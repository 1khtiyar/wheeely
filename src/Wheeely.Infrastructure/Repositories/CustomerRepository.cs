﻿
using Wheeely.Domain.Entities;
using Wheeely.Infrastructure.Contracts.Repositories;
using Wheeely.Infrastructure.Data;

namespace Wheeely.Infrastructure.Repositories
{
    public class CustomerRepository : BaseRepository<CustomerEntity>, ICustomerRepository
    {
        public CustomerRepository(CoreContext coreContext) : base(coreContext)
        {

        }
    }
}
