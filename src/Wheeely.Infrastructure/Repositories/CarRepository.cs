﻿
using Wheeely.Domain.Entities;
using Wheeely.Infrastructure.Contracts.Repositories;
using Wheeely.Infrastructure.Data;

namespace Wheeely.Infrastructure.Repositories
{
    public class CarRepository : BaseRepository<CarEntity>, ICarRepository
    {
        public CarRepository(CoreContext coreContext) : base(coreContext)
        {

        }
    }
}
