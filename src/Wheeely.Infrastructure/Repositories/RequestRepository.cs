﻿
using Wheeely.Domain.Entities;
using Wheeely.Infrastructure.Contracts.Repositories;
using Wheeely.Infrastructure.Data;

namespace Wheeely.Infrastructure.Repositories
{
    public class RequestRepository : BaseRepository<RequestEntity>, IRequestRepository
    {
        public RequestRepository(CoreContext coreContext) : base(coreContext)
        {

        }
    }
}
