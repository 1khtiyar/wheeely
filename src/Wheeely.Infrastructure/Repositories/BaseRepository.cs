﻿
using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Wheeely.Domain.Entities;
using Wheeely.Infrastructure.Contracts.Repositories;
using Wheeely.Infrastructure.Data;

namespace Wheeely.Infrastructure.Repositories
{
    public class BaseRepository<TEntity> : IAsyncRepository<TEntity>
        where TEntity : BaseEntity
    {
        private readonly CoreContext _coreContext;

        public BaseRepository(CoreContext coreContext)
        {
            _coreContext = coreContext;
        }

        public async Task<TEntity> CreateAsync(TEntity entity)
        {
            _coreContext.Set<TEntity>().Add(entity);
            await _coreContext.SaveChangesAsync();
            return entity;
        }

        public async Task DeleteAsync(TEntity entity)
        {
            _coreContext.Set<TEntity>().Remove(entity);
            await _coreContext.SaveChangesAsync();
        }

        public async Task<IReadOnlyList<TEntity>> GetAsync()
        {
            return await _coreContext.Set<TEntity>().ToListAsync();
        }

        public async Task<IReadOnlyList<TEntity>> GetAsync(Func<TEntity, bool> predicate)
        {
            IQueryable<TEntity> query = _coreContext.Set<TEntity>();
            
            if (predicate != null)
            {
                query = query.Where(predicate) as IQueryable<TEntity>;
            }

            return await query.ToListAsync();
        }

        public async Task<TEntity> GetAsync(int id)
        {
            return await _coreContext.Set<TEntity>().FindAsync(id);
        }

        public async Task UpdateAsync(TEntity entity)
        {
            _coreContext.Entry(entity).State = EntityState.Modified;
            await _coreContext.SaveChangesAsync();
        }
    }
}
