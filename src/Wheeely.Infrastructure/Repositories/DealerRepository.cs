﻿
using Wheeely.Domain.Entities;
using Wheeely.Infrastructure.Contracts.Repositories;
using Wheeely.Infrastructure.Data;

namespace Wheeely.Infrastructure.Repositories
{
    public class DealerRepository : BaseRepository<DealerEntity>, IDealerRepository
    {
        public DealerRepository(CoreContext coreContext) : base(coreContext)
        {

        }
    }
}
