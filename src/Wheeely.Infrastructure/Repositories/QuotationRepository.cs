﻿
using Wheeely.Domain.Entities;
using Wheeely.Infrastructure.Contracts.Repositories;
using Wheeely.Infrastructure.Data;

namespace Wheeely.Infrastructure.Repositories
{
    public class QuotationRepository : BaseRepository<QuotationEntity>, IQuotationRepository
    {
        public QuotationRepository(CoreContext coreContext) : base(coreContext)
        {

        }
    }
}
