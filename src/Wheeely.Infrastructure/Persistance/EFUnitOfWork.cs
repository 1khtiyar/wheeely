﻿
using Wheeely.Infrastructure.Contracts.Persistance;
using Wheeely.Infrastructure.Contracts.Repositories;
using Wheeely.Infrastructure.Data;
using Wheeely.Infrastructure.Repositories;

namespace Wheeely.Infrastructure.Persistance
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private readonly CoreContext _coreContext;
        private ICarRepository _carRepository;
        private ICustomerRepository _customerRepository;
        private IDealerRepository _dealerRepository;
        private IQuotationRepository _quotationRepository;
        private IRequestRepository _requestRepository;

        public ICarRepository CarRepository
        {
            get
            {
                if (_carRepository is null)
                {
                    _carRepository = new CarRepository(_coreContext);
                }
                return _carRepository;
            }
        }

        public ICustomerRepository CustomerRepository
        {
            get
            {
                if (_customerRepository is null)
                {
                    _customerRepository = new CustomerRepository(_coreContext);
                }
                return _customerRepository;
            }
        }

        public IDealerRepository DealerRepository
        {
            get
            {
                if (_dealerRepository is null)
                {
                    _dealerRepository = new DealerRepository(_coreContext);
                }
                return _dealerRepository;
            }
        }

        public IQuotationRepository QuotationRepository
        {
            get
            {
                if (_quotationRepository is null)
                {
                    _quotationRepository = new QuotationRepository(_coreContext);
                }
                return _quotationRepository;
            }
        }

        public IRequestRepository RequestRepository
        {
            get
            {
                if (_requestRepository is null)
                {
                    _requestRepository = new RequestRepository(_coreContext);
                }
                return _requestRepository;
            }
        }

        public EFUnitOfWork(CoreContext coreContext)
        {
            _coreContext = coreContext;
        }
    }
}
