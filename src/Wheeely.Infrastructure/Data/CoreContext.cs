﻿using Microsoft.EntityFrameworkCore;

using Wheeely.Domain.Entities;
using Wheeely.Infrastructure.Configurations;

namespace Wheeely.Infrastructure.Data
{
    public class CoreContext : DbContext
    {
        public DbSet<CustomerEntity> Customers { get; set; }
        public DbSet<DealerEntity> Dealers { get; set; }
        public DbSet<QuotationEntity> Quotations { get; set; }
        public DbSet<RequestEntity> Requests { get; set; }
        public DbSet<CarEntity> Cars { get; set; }
        public DbSet<CurrencyEntity> Currencies { get; set; }

        public CoreContext(DbContextOptions<CoreContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CustomerEntityConfiguration());
            modelBuilder.ApplyConfiguration(new DealerEntityConfiguration());
            modelBuilder.ApplyConfiguration(new CarEntityConfiguration());
            modelBuilder.ApplyConfiguration(new RequestEntityConfiguration());
            modelBuilder.ApplyConfiguration(new QuotationEntityConfiguration());
            modelBuilder.ApplyConfiguration(new CurrencyEntityConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }
}
