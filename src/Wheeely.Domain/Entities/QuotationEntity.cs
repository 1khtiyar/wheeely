﻿using System.Collections.Generic;

namespace Wheeely.Domain.Entities
{
    public class QuotationEntity : IdEntity
    {
        public int CarID { get; set; }
        public double Price { get; set; }
        public int CurrencyID { get; set; }

        public virtual CarEntity Car { get; set; }
        public virtual CurrencyEntity Currency { get; set; }
        public virtual ICollection<RequestEntity> Requests { get; set; }
    }
}
