﻿using System.Collections.Generic;

namespace Wheeely.Domain.Entities
{
    public class DealerEntity : IdEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public virtual ICollection<CarEntity> Cars { get; set; }
    }
}
