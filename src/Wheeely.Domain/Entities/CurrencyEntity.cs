﻿using System.Collections.Generic;

namespace Wheeely.Domain.Entities
{
    public class CurrencyEntity : IdEntity
    {
        public string Code { get; set; }
        public string Symbol { get; set; }

        public virtual ICollection<QuotationEntity> Quotations { get; set; }
    }
}
