﻿namespace Wheeely.Domain.Entities
{
    public class IdEntity : BaseEntity
    {
        public int ID { get; set; }
    }
}
