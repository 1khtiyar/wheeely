﻿using System.Collections.Generic;

namespace Wheeely.Domain.Entities
{
    public class CustomerEntity : IdEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public virtual ICollection<RequestEntity> Requests { get; set; }
    }
}
