﻿using System.Collections.Generic;

namespace Wheeely.Domain.Entities
{
    public class CarEntity : IdEntity
    {
        public string Name { get; set; }
        public int Year { get; set; }
        public int OwnerID { get; set; }

        public virtual DealerEntity Owner { get; set; }
        public virtual QuotationEntity Quotation { get; set; }
    }
}
