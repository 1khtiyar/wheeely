﻿namespace Wheeely.Domain.Entities
{
    public class RequestEntity : IdEntity
    {
        public int CustomerID { get; set; }
        public int QuotationID { get; set; }
        public string Message { get; set; }

        public virtual CustomerEntity Customer { get; set; }
        public virtual QuotationEntity Quotation { get; set; }
    }
}
