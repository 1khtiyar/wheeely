﻿
using Wheeely.Domain.Entities;

namespace Wheeely.Infrastructure.Contracts.Repositories
{
    public interface ICustomerRepository : IAsyncRepository<CustomerEntity>
    {

    }
}
