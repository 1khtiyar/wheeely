﻿
using Wheeely.Domain.Entities;

namespace Wheeely.Infrastructure.Contracts.Repositories
{
    public interface IDealerRepository : IAsyncRepository<DealerEntity>
    {

    }
}
