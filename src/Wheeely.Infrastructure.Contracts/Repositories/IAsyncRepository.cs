﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Wheeely.Domain.Entities;

namespace Wheeely.Infrastructure.Contracts.Repositories
{
    public interface IAsyncRepository<TEntity>
        where TEntity : BaseEntity
    {
        Task<IReadOnlyList<TEntity>> GetAsync();
        Task<IReadOnlyList<TEntity>> GetAsync(Func<TEntity, bool> predicate);
        Task<TEntity> GetAsync(int id);
        Task<TEntity> CreateAsync(TEntity entity);
        Task UpdateAsync(TEntity entity);
        Task DeleteAsync(TEntity entity);
    }
}
