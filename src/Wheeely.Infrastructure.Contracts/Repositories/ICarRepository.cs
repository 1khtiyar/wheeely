﻿
using Wheeely.Domain.Entities;

namespace Wheeely.Infrastructure.Contracts.Repositories
{
    public interface ICarRepository : IAsyncRepository<CarEntity>
    {

    }
}
