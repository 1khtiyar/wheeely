﻿using Wheeely.Infrastructure.Contracts.Repositories;

namespace Wheeely.Infrastructure.Contracts.Persistance
{
    public interface IUnitOfWork
    {
        ICarRepository CarRepository { get; }
        ICustomerRepository CustomerRepository { get; }
        IDealerRepository DealerRepository { get; }
        IQuotationRepository QuotationRepository { get; }
        IRequestRepository RequestRepository { get; }
    }
}
