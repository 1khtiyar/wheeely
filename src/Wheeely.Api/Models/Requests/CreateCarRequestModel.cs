﻿namespace Wheeely.Api.Models.Requests
{
    public class CreateCarRequestModel
    {
        public int OwnerID { get; set; }
        public string Name { get; set; }
        public int Year { get; set; }
    }
}
