﻿namespace Wheeely.Api.Models.Requests
{
    public class CreateRequestEntityRequestModel
    {
        public int QuotationID { get; set; }
        public int CustomerID { get; set; }
        public string Message { get; set; }
    }
}
