﻿namespace Wheeely.Api.Models.Requests
{
    public class CreateQuotationRequestModel
    {
        public int CarID { get; set; }
        public double Price { get; set; }
        public int CurrencyID { get; set; }
    }
}
