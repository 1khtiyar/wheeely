﻿using AutoMapper;

using Wheeely.Api.DTO;
using Wheeely.Api.Models.Requests;
using Wheeely.Domain.Entities;

namespace Wheeely.Api.Utils.MappingProfiles
{
    public class CarProfile : Profile
    {
        public CarProfile()
        {
            CreateMap<CarEntity, CarDTO>().ReverseMap();

            CreateMap<CreateCarRequestModel, CarEntity>();
        }
    }
}
