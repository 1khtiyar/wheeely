﻿using Microsoft.Extensions.DependencyInjection;

using Wheeely.Api.Utils.MappingProfiles;

namespace Wheeely.Api.Utils.Services
{
    public static class AutoMapperExtension
    {
        public static void AddMapper(this IServiceCollection services)
        {
            services.AddSingleton(new AutoMapper.MapperConfiguration(m =>
            {
                m.AddProfile<CarProfile>();
            }).CreateMapper());
        }
    }
}
