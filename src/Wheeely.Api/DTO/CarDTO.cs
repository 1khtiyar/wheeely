﻿namespace Wheeely.Api.DTO
{
    public class CarDTO
    {
        public string Name { get; set; }
        public int Year { get; set; }
        public int OwnerID { get; set; }
    }
}
