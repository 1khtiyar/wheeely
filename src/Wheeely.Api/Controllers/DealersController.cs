﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.Threading.Tasks;

using Wheeely.Domain.Entities;
using Wheeely.Infrastructure.Contracts.Persistance;

namespace Wheeely.Api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class DealersController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public DealersController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        [ProducesResponseType(typeof(ICollection<DealerEntity>), 200)]
        public async Task<ActionResult<ICollection<DealerEntity>>> GetDealers()
        {
            return Ok(await _unitOfWork.DealerRepository.GetAsync());
        }

        [HttpGet("{id}", Name = "GetDealerByID")]
        [ProducesResponseType(typeof(DealerEntity), 200)]
        public async Task<ActionResult<DealerEntity>> GetDealer(int id)
        {
            var dealer = await _unitOfWork.DealerRepository.GetAsync(id);

            if (dealer is null)
            {
                return NotFound();
            }

            return Ok(dealer);
        }

        [HttpPost]
        [ProducesResponseType(typeof(DealerEntity), StatusCodes.Status201Created)]
        public async Task<ActionResult<DealerEntity>> CreateDealer([FromBody] DealerEntity dealerEntity)
        {
            await _unitOfWork.DealerRepository.CreateAsync(dealerEntity);

            return CreatedAtRoute("GetDealerByID", new { id = dealerEntity.ID }, dealerEntity);
        }
    }
}
