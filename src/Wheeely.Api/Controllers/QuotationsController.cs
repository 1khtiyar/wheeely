﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.Threading.Tasks;

using Wheeely.Domain.Entities;
using Wheeely.Infrastructure.Contracts.Persistance;

namespace Wheeely.Api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class QuotationsController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public QuotationsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        [HttpGet]
        [ProducesResponseType(typeof(ICollection<QuotationEntity>), StatusCodes.Status200OK)]
        public async Task<ActionResult<ICollection<QuotationEntity>>> GetQuotations()
        {
            return Ok(await _unitOfWork.QuotationRepository.GetAsync());
        }

        [HttpGet("{carId}", Name = "GetQuotationByCarID")]
        [ProducesResponseType(typeof(QuotationEntity), StatusCodes.Status200OK)]
        public async Task<ActionResult<QuotationEntity>> GetQuotation(int carId)
        {
            var quotation = await _unitOfWork.QuotationRepository.GetAsync(carId);

            if (quotation is null)
            {
                return NotFound();
            }

            return Ok(quotation);
        }

        [HttpPost]
        [ProducesResponseType(typeof(QuotationEntity), StatusCodes.Status201Created)]
        public async Task<ActionResult<QuotationEntity>> CreateRequest([FromBody] QuotationEntity quotationEntity)
        {
            var referencedCar = await _unitOfWork.CarRepository.GetAsync(quotationEntity.CarID);

            if (referencedCar != null)
            {
                return BadRequest("Already exists");
            }

            await _unitOfWork.QuotationRepository.CreateAsync(quotationEntity);
            return CreatedAtRoute("GetQuotationByCarID", new { carId = quotationEntity.CarID }, quotationEntity);
        }

        [HttpPut]
        [ProducesResponseType(typeof(QuotationEntity), StatusCodes.Status200OK)]
        public async Task<ActionResult<QuotationEntity>> UpdateRequest([FromBody] QuotationEntity quotationEntity)
        {
            var srcQuotation = await _unitOfWork.QuotationRepository.GetAsync(quotationEntity.CarID);

            if (srcQuotation is null)
            {
                return NotFound();
            }

            await _unitOfWork.QuotationRepository.UpdateAsync(quotationEntity);

            return Ok();
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(QuotationEntity), StatusCodes.Status201Created)]
        public async Task<ActionResult<QuotationEntity>> DeleteRequest(int id)
        {
            var quotationEntity = await _unitOfWork.QuotationRepository.GetAsync(id);

            if (quotationEntity is null)
            {
                return NotFound();
            }

            await _unitOfWork.QuotationRepository.DeleteAsync(quotationEntity);

            return Ok();
        }
    }
}
