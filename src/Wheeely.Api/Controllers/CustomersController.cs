﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.Threading.Tasks;

using Wheeely.Domain.Entities;
using Wheeely.Infrastructure.Contracts.Persistance;

namespace Wheeely.Api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public CustomersController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        [ProducesResponseType(typeof(ICollection<CustomerEntity>), 200)]
        public async Task<ActionResult<ICollection<CustomerEntity>>> GetCustomers()
        {
            return Ok(await _unitOfWork.CustomerRepository.GetAsync());
        }

        [HttpGet("{id}", Name = "GetCustomerByID")]
        [ProducesResponseType(typeof(CustomerEntity), 200)]
        public async Task<ActionResult<CustomerEntity>> GetCustomer(int id)
        {
            var customerEntity = await _unitOfWork.CustomerRepository.GetAsync(id);

            if (customerEntity is null)
            {
                return NotFound();
            }

            return Ok(customerEntity);
        }

        [HttpPost]
        [ProducesResponseType(typeof(CustomerEntity), StatusCodes.Status201Created)]
        public async Task<ActionResult<CustomerEntity>> CreateCustomer([FromBody] CustomerEntity customerEntity)
        {
            await _unitOfWork.CustomerRepository.CreateAsync(customerEntity);

            return CreatedAtRoute("GetCustomerByID", new { id = customerEntity.ID }, customerEntity);
        }
    }
}
