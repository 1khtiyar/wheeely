﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.Threading.Tasks;

using Wheeely.Domain.Entities;
using Wheeely.Infrastructure.Contracts.Persistance;

namespace Wheeely.Api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class RequestsController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public RequestsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        [ProducesResponseType(typeof(ICollection<RequestEntity>), StatusCodes.Status200OK)]
        public async Task<ActionResult<ICollection<RequestEntity>>> GetRequests()
        {
            return Ok(await _unitOfWork.RequestRepository.GetAsync());
        }

        [HttpGet("{id}", Name = "GetRequestByID")]
        [ProducesResponseType(typeof(RequestEntity), StatusCodes.Status200OK)]
        public async Task<ActionResult<RequestEntity>> GetRequest(int id)
        {
            var request = await _unitOfWork.RequestRepository.GetAsync(id);

            if (request is null)
            {
                return NotFound();
            }

            return Ok(request);
        }

        [HttpPost]
        [ProducesResponseType(typeof(RequestEntity), StatusCodes.Status201Created)]
        public async Task<ActionResult<RequestEntity>> CreateRequest([FromBody] RequestEntity requestEntity)
        {
            await _unitOfWork.RequestRepository.CreateAsync(requestEntity);
            return CreatedAtRoute("GetRequestByID", new { id = requestEntity.CustomerID }, requestEntity);
        }

        [HttpPut]
        [ProducesResponseType(typeof(RequestEntity), StatusCodes.Status200OK)]
        public async Task<ActionResult<RequestEntity>> UpdateRequest([FromBody] RequestEntity requestEntity)
        {
            var srcRequest = await _unitOfWork.RequestRepository.GetAsync(requestEntity.ID);

            if (srcRequest is null)
            {
                return NotFound();
            }

            await _unitOfWork.RequestRepository.UpdateAsync(requestEntity);

            return Ok();
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(RequestEntity), StatusCodes.Status201Created)]
        public async Task<ActionResult<RequestEntity>> DeleteRequest(int id)
        {
            var requestEntity = await _unitOfWork.RequestRepository.GetAsync(id);
            
            if (requestEntity is null)
            {
                return NotFound();
            }

            await _unitOfWork.RequestRepository.DeleteAsync(requestEntity);
            
            return Ok();
        }
    } 
}
