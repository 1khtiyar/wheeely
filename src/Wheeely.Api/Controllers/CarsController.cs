﻿using AutoMapper;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Wheeely.Api.DTO;
using Wheeely.Api.Models.Requests;
using Wheeely.Domain.Entities;
using Wheeely.Infrastructure.Contracts.Persistance;

namespace Wheeely.Api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class CarsController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public CarsController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        [ProducesResponseType(typeof(CarDTO), 200)]
        public async Task<ActionResult<ICollection<CarDTO>>> GetCars()
        {
            var cars = await _unitOfWork.CarRepository.GetAsync();
            return Ok(cars.Select(c => _mapper.Map<CarDTO>(c)));
        }

        [HttpGet("{id}", Name = "GetCarByID")]
        [ProducesResponseType(typeof(CarDTO), 200)]
        public async Task<ActionResult<CarDTO>> GetCar(int id)
        {
            var carEntity = await _unitOfWork.CarRepository.GetAsync(id);

            if (carEntity is null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<CarDTO>(carEntity));
        }

        [HttpPost]
        [ProducesResponseType(typeof(CarEntity), StatusCodes.Status201Created)]
        public async Task<ActionResult<CarEntity>> CreateCar([FromBody] CreateCarRequestModel request)
        {
            // can insert validation

            var carEntity = _mapper.Map<CarEntity>(request);
            await _unitOfWork.CarRepository.CreateAsync(carEntity);

            return CreatedAtRoute("GetCarByID", new { id = carEntity.ID }, carEntity);
        }
    }
}
