# Wheeely

Car Sale System



## Tasks

Define car sales system on ASP.net core API
The dealer must enter his car information
After registering the car information, the seller must specify the price of each car.
After searching, the customer selects the desired car and sends it to the seller in the form of a request



## Architecture

[Project](./docs/Architecture.drawio) - docs/Architecture.draw.io (Download file and open in [draw.io](https://app.diagrams.net/) web app)

[C4 Model](./docs/Architecture - C4 Model.pdf) - docs/Architecture - C4 Model.pdf

[General diagrams](./docs/Architecture - General diagrams.pdf) - ./docs/Architecture - General diagrams.pdf

[Use cases diagrams](./docs/Architecture - Use cases diagrams.pdf) - ./docs/Architecture - Use cases diagrams.pdf



## Development

Read about [development](./docs/Development.md) details



## Project status
Thinkin about new features)

